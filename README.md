# web-less

A budget readability web parser-reader that works from the command line.

In other words, a ghetto [Mercury Reader](https://mercury.postlight.com/reader/) or a [Reader View](https://blog.mozilla.org/firefox/reader-view/) for the terminal users.

## How to install

`web-less` is a standard `npm` module that can be found found [here](https://www.npmjs.com/package/web-less).

To install on your system

1. Login as root
``` su ```

2. Install `web-less`
```npm install -g web-less```

## Usage

![](demo.gif)

### Simply open a webpage as a PDF

```
web-less https://stallman.org/stallman-computing.html
```

### Save Webpage or PDF 

```
web-less --mode save https://stallman.org/stallman-computing.html > file.pdf
```
```
web-less --m webpage https://stallman.org/stallman-computing.html > file.html
```

### Input Webpage from a file

```
web-less -input file.html
```

### Input Webpage from STDIN

```
curl 'https://stallman.org/stallman-computing.html' | web-less -i -
```


