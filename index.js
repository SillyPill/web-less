#!/usr/bin/env node

import { readFileSync, createWriteStream } from "fs";
import Mercury from "@postlight/mercury-parser";
import minimist from "minimist";
import open from "open";
import { fileSync } from "tmp";
import { basename } from "path";
import wkhtmltopdf from "wkhtmltopdf";

const args = minimist(process.argv.slice(2), {
  string: ["mode", "input", "stylesheet"],
  boolean: ["h", "v"],
  alias: { h: "help", v: "version", m: "mode", i: "input", c: "stylesheet" },
});

if (args.v) {
  console.log(require("./package.json").version);
  process.exit(0);
}

if (args.h) {
  print_help();
  process.exit(0);
}

if (args.mode == undefined) {
  args.mode = "open";
}

function print_help() {
  const command = basename(process.argv[1]);
  const help_text = `Usage: ${command} [OPTIONS]... URL
Pager for reading de-bloated webpages as a PDF

Options:
  -m, --mode=<option>   Available options are 'open', 'save', and 'webpage';
            'open' shows a temporary pdf file.
            'save' prints the pdf stream to STDOUT,
            'webpage' prints readability webpage HTML to STDOUT; 
            default is 'open'
  -i, --input       Input from a file instead of a URL. '-' to input from
                    STDIN.
  -v, --version     Print version of the script and exit.
  -h, --help        Show this help message and exit.
    `;
  console.error(help_text);
}

function getStyleSheet() {
  if (args.stylesheet !== undefined) {
    return readFileSync(args.stylesheet, { encoding: "utf8" });
  } else {
    return `
body{
    background-color : #FFF !important;
    font-size : 14pt !important;
    text-align : justify;
    line-height: 1.5;
}
@media print {
    background: transparent !important;
}
iframe{
    display : block;
    height : 300px;
}
img{
    display : block;
    margin : 20px auto;
}
h1 {
    text-align : left !important;
}
h2 {
    text-align : left !important;
    font-size : 16pt !important;
}
h1 a{
    color : black !important;
    text-decoration : none !important;
}
a:link{
    color : #333 !important;
}
    `;
  }
}

function articleToHtml(article, uri) {
  return `
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>
            ${article.title}
        </title>
        <style>${getStyleSheet()}</style>
    </head>
    <body>
        <h1>
            ${uri ? '<a href="' + uri + '">' : ""}
            ${article.title}
            ${uri ? "</a>" : ""}
        </h1>
        ${article.content}
    </body>
</html>`;
}

function htmlToPdfStream(htmlContent) {
  const pdfStream = wkhtmltopdf(htmlContent, {
    pageSize: "A4",
    "margin-top": 30,
    "margin-bottom": 30,
    "margin-left": 30,
    "margin-right": 30,
    "disable-javascript": true,
    zoom: 1,
  });

  return pdfStream;
}

function doOutput(htmlContent, mode) {
  switch (mode) {
    case "save":
      {
        const pdfStream = htmlToPdfStream(htmlContent);
        pdfStream.pipe(process.stdout);

        pdfStream.on("error", function (err) {
          console.error(err);
        });
      }
      break;

    case "webpage":
      console.log(htmlContent);
      process.exit(0);

    default:
      console.error(
        `${args.mode}' is not a valid arguement for '--mode'. Defaulting to 'open' `
      );

    case "open":
      {
        const tmpobj = fileSync({ postfix: ".pdf" });
        const pdfStream = htmlToPdfStream(htmlContent);
        pdfStream.pipe(createWriteStream(tmpobj.name));

        pdfStream.on("end", function () {
          (async () => {
            await open(tmpobj.name, { wait: true });
            tmpobj.removeCallback();
          })();
        });

        pdfStream.on("error", function (err) {
          console.error(err);
        });
      }
      break;
  }
}

// if provided URL, give it to Mercury API
// else input is defined, use stdin

try {
  if (args.input === undefined) {
    if (args._[0] == undefined) {
      console.error("No URL provided...\n");
      print_help();
      process.exit(0);
    }
    const uri = args._[0];

    // Pass dom to the readability library
    Mercury.parse(uri).then(
      (article) => {
        doOutput(articleToHtml(article, uri), args.mode);
      },
      (error) => {
        console.error(error.message);
      }
    );
  } else {
    if (args.input == "-") {
      var src = readFileSync(0, "utf-8");
    } else {
      var src = readFileSync(args.input, "utf-8");
    }

    console.time("parser");
    Mercury.parse("http://www.no-url-provided.workaround", {
      html: src,
    }).then(
      (article) => {
        console.timeEnd("parser");
        doOutput(articleToHtml(article), args.mode);
      },
      (error) => {
        console.error(error.message);
      }
    );
  }
} catch (e) {
  console.error(e.message);
}
